(function(){
  'use strict';

  angular.module('app.core')
    .factory('webSocket', webSocket);


  function webSocket(){
    var webSocketObj;
    return{
      socket: webSocketObj,
      open: function(){
        var host = "127.0.0.1";
        //checking if we access to the hmi in remote, in this case we need to update the host variable
        var addrRegExp = /[^\/]+\/\/(.+):.+/gi;
        var match = addrRegExp.exec(window.location.href);
        if(match !== null){
          host = match[1];
        }

        this.socket = io("http://"+host+":8181/client");
      }
    };
  }
})();