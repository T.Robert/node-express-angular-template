﻿(function(){
  'use strict';

  angular.module('app.home')
    .controller('HomeController', homeController);
  
  homeController.$inject = ['$scope', '$timeout', 'webSocket'];
  function homeController($scope, $timeout, webSocket) {
    /* jshint validthis: true */
    var $ctrl = this;

    //-------------------------------------------------------------------------------------------------------------------
    //TRIGGERED FUNCTIONS
    //-------------------------------------------------------------------------------------------------------------------
    

    //-------------------------------------------------------------------------------------------------------------------
    //MESSAGING
    //-------------------------------------------------------------------------------------------------------------------

    $ctrl.initCom = function(){
    	$ctrl.connectionStatus = 'KO';
      webSocket.open();
      //register socket event handlers
      webSocket.socket.on('connect', function () {
        console.log("socket connected");
        $timeout(function(){$ctrl.connectionStatus = 'OK';});
      });

      webSocket.socket.on('disconnect', function () {
        console.log("socket disconnected");
        $timeout(function(){$ctrl.connectionStatus = 'KO';});
      });

      webSocket.socket.on('successMsg', function (msg, title) {
        toastr.success(msg, title);
      });

      webSocket.socket.on('infoMsg', function (msg, title) {
        toastr.info(msg, title);
      });

      webSocket.socket.on('errorMsg', function (msg, title) {
        toastr.error(msg, title);
      });
    };

    //-------------------------------------------------------------------------------------------------------------------
    //TOOLS
    //-------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------
    //INIT
    //-------------------------------------------------------------------------------------------------------------------
    var init = function(){
      $ctrl.initCom();
    };
    init();
  }

})();