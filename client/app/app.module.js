(function(){
  'use strict';

  angular.module('app', [
    'ui.router',
    'ngMaterial',

    'app.core',
    'app.home',
    'app.page'
  ]);

  // config default route
  angular.module('app')
    .config(['$urlRouterProvider', function ($urlRouterProvider) {
      // routing config
      $urlRouterProvider.otherwise('/home/page');
      // toastr config
      toastr.options.progressBar = true;
      toastr.options.preventDuplicates = false;
      toastr.options.newestOnTop = false;
    }]);
})();