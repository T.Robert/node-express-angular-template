(function(){
  'use strict';

  angular.module('app.page')
  .config(['$stateProvider', function($stateProvider){
    $stateProvider
      .state('home.page', {
        url: '/page',
        templateUrl: 'app/page/page.html',
        controller: 'PageController',
        controllerAs: '$ctrl'
      });
  }]);
})();