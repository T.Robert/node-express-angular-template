var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var serverWeb = require('http').createServer(app);
var io = require('socket.io')(serverWeb);
var net = require('net');
var fs = require('fs-extra');
var winston = require('winston');

//-----------------------------------------------------------------
//LOGGING CONFIGURATION
//-----------------------------------------------------------------
fs.ensureDirSync("./logs/");
var logger = new winston.Logger({
  transports: [
    new winston.transports.File({
      level: 'info',
      filename: './logs/server.log',
      maxsize: 5242880, //5MB
      maxFiles: 10,
      json: false,
      colorize: false,
      timestamp: function() {
        return new Date().toLocaleString();
      },
      formatter: function(options) {
        // Return string will be passed to logger.
        return options.timestamp() +' '+ options.level.toUpperCase() +' '+ (options.message ? options.message : '');
      }
    }),
    new winston.transports.Console({
      level: 'info',
      json: false,
      colorize: true,
      timestamp: function() {
        return new Date().toLocaleString();
      },
      formatter: function(options) {
        // Return string will be passed to logger.
        return options.timestamp() +' '+ options.level.toUpperCase() +' '+ (options.message ? options.message : '');
      }
    })
  ],
  exceptionHandlers:[
  	new winston.transports.File({ filename: './logs/exception.log' }),
  	new winston.transports.Console({json: true})
  ],
  exitOnError: false
});

//-----------------------------------------------------------------
// SERVER DEFINITION
//-----------------------------------------------------------------

//WEB SERVER
// parse application/json in http req
app.use(bodyParser.json());
//define the accessible folder
app.use(express.static('client'));

var clientNamespace = io.of('/client');
//setup clientNamespace event handlers²
clientNamespace.on('connection', function(client){
	logger.info('client ('+client.handshake.address.replace("::ffff:","")+') connected');
	//registering the event handler (message received) for this socket

	//registering a disconnect event for this specific socket
	client.on('disconnect', function(){
		logger.info('client ('+client.handshake.address.replace("::ffff:","")+') disconnected');
	});
});




//default get to identify the web server in a browser
app.get('/', function (req, res) {
	res.sendFile( __dirname + "/client/Index.html");
	console.log(__dirname);
});

//Starting the web server
serverWeb.listen(8181, function () {
	console.log('Web server listening on port 8181!');
});


//-----------------------------------------------------------------
// TOOLS
//-----------------------------------------------------------------
