# Template Node App
This is a template for a Node app with :
* an Express server
* a logger (Winston)
* a websocket server (Socket.io)
* an Angular (1.x) app for the client side with :
    * Bootstrap
    * Angular Material
    * Toastr
    * Socket.io
    * Moment.js

# Setup
* `npm install` in the root folder

# Run
* `npm start` in the root folder